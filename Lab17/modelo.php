<?php

function connectDB(){
  $servername= "localhost";
  $username = "root";
  $password = "root";
  $dbname = "Laboratorio";

  $mysql = mysqli_connect($servername, $username, $password, $dbname);

  // Check connection
  if (!$mysql) {
      die("Connection failed: " . mysqli_connect_error());
  }
  return $mysql;
}

function closeDB($mysql){
  mysqli_close($mysql);

}

function getCursos($Cursos){
    $db = connectDB();
    $sql = 'SELECT * FROM '.$Cursos;
    $result = $db->query($sql);
    while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)){
        for($i=0; $i<count($row); $i++){
            echo $fila[$i].' ';
        }
        echo '<br>';
    }
    echo '<br>';
    mysqli_free_result($result);
    closeDB($db);
}

function getNombreCurso($db, $Id_Curso)
{
    //Specification of the SQL query
    $query='SELECT Nombre_Curso FROM Cursos WHERE Nombre_Curso="'.$Nombre_Curso.'"';
    // Query execution; returns identifier of the result group
    $result = $db->query($query);   
    $fila = mysqli_fetch_array($result, MYSQLI_BOTH);
    $nombre = $fila["Nombre_Curso"];
    return $nombre;
}

function getNombreCursos(){
    $db = connectDb();
    $sql = "SELECT Nombre_Curso  FROM Cursos";
    $result = $db->query($sql);
    $table = "
    <table border = '1px'>
        <thread>
            <tr>
                <th>Curso</th>
            </tr>
        </thread>
        <tbody>";

    while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)){

        $table .= '
          <tr>
              <td>'.$row["Nombre_Curso"].'</td>
          </tr>';
    }

    mysqli_free_result($result);
    closeDb($db);
    $table .= "</tbody></table>";
    echo "<br>";
    return $table;
}

function getFecha(){
    $db = connectDb();
    $sql = "SELECT Nombre_Curso, Precio, Fecha, Objetivo FROM Cursos WHERE Fecha between '2017/10/01' AND '2017/10/31'";
    $result = $db->query($sql);
    echo "<br>";
    $table = "
    <table border = '1px'>
        <thread>
            <tr>
                <th>Curso</th>
                <th>Precio</th>
                <th>Fecha</th>
                <th>Objetivo</th>
            </tr>
        </thread>
        <tbody>";

    while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)){

        $table .= '
          <tr>
              <td>'.$row["Nombre_Curso"].'</td>
              <td>'.$row["Precio"].'</td>
              <td>'.$row["Fecha"].'</td>
              <td>'.$row["Objetivo"].'</td>
          </tr>';
    }

    mysqli_free_result($result);
    closeDb($db);
    $table .= "</tbody></table>";
    return $table;
}

function getConsultarCursos() {
    $db = connectDB();
    //Specification of the SQL query
    $query='SELECT * FROM Cursos ORDER BY Fecha asc';
     // Query execution; returns identifier of the result group
    $result = $db->query($query);
    $cuadros = "";
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
    $cuadros .= '
    <div class="row wow fadeInLeft animated" data-wow-offset="30" data-wow-duration="1.5s" data-wow-delay="0.15s">
    <div class="col-md-3">
    <div class="item item-1" style="background-image: url(images/products/'.$fila["Id_Curso"].'.jpg);">
        <div class="item-overlay">
        </div>
        <div class="item-content">
            <div class="item-top-content">
                <div class="item-top-content-inner">
                    <div class="item-product">
                      <div class="item-top-title">
                        <span class="item-top-title">'.$fila["Nombre_Curso"].'</span>
                                 <p>'.$fila["Objetivo"].'</p>
                                 <p>'.$fila["Fecha"].'</p>
                                 <p class="subdescription">
                            </p> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-add-content">
                <div class="item-add-content-inner">
                    <div class="section">
                        <p>
                        <p>'.$fila["Precio"].'</p>
                        </p>
                    </div>
                    <div class="section">
                        <a class=class="btn btn-primary custom-button red-btn" href="editar.php?id='.$fila["id"].'">'.Editar.'</a>
                        <a href="#" class="btn btn-primary custom-button red-btn">Inscribir</a><br/>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
</div>';
}
    // it releases the associated results
    mysqli_free_result($result);
    closeDB($db);
    return $cuadros;
}


function getDropdownCursos($table, $order_by_field, $selected=-1) {
    $db = connectDb();
    //Specification of the SQL query
    $query='SELECT * FROM '.$table.' ORDER BY '.$order_by_field;
     // Query execution; returns identifier of the result group
    $result = $db->query($query);
    $select = '<select name="Nombre_Curso" id="Nombre_Curso">';
     // cycle to explode every line of the results
    while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        if ($selected == $row["Id_Curso"]) {
            $select .= '<option value="'.$row["Nombre_Curso"].'" selected>'.$row["$order_by_field"].'</option>';
        } else {
            $select .= '<option value="'.$row["Nombre_Curso"].'">'.$row["$order_by_field"].'</option>';
        }
    }
    $select .= '</select><label for="'.$table.'">Nombre_Curso</label>';
    // it releases the associated results
    mysqli_free_result($result);
    closeDb($db);
    return $select;
}

function guardarCurso($Nombre_Curso, $Objetivo){
    $db = connectDB();
    // insert command specification 
    $query='INSERT INTO Cursos (`Nombre_Curso`,`Objetivo`) VALUES (?,?) ';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("ss", $Nombre_Curso, $Objetivo)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // Executing the statement
    if (!$statement->execute()) {
        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
     } 
    closeDB($db);
}

function editaCurso($Nombre_Curso, $Objetivo)
{
    $db = connectDB();
    
    // insert command specification 
    $query='UPDATE Cursos SET Nombre_Curso=?, Objetivo=? WHERE Id_Curso=?';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("ss", $Nombre_Curso, $Objetivo)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // update execution
    if ($statement->execute()) {
        echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
    } else {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }
         
    closeDB($db);
}

function getRegistro($db, $Id_Curso)
{
//Specification of the SQL query
$query='SELECT * FROM Cursos WHERE Id_Curso ="'.$Id_Curso.'"';
 // Query execution; returns identifier of the result group
$result = $db->query($query);   
$fila = mysqli_fetch_array($result, MYSQLI_BOTH);
return $fila; 
}


function getNombreCur()
{
    $db = connectDB();
    //Specification of the SQL query
    $query='SELECT * FROM Cursos';
     // Query execution; returns identifier of the result group
    $result = $db->query($query);
    //$arreglo = "";
    $i = 1;
    $arreglo = array();
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        
        $arreglo [$i] = getNombreCurso($db, $i);
            //= $fila["nombre_insumo"];


        $i = $i + 1;    
    }
    
    //$asd = print_r($arreglo);

    // it releases the associated results
    mysqli_free_result($result);
    closeDB($db);	    
    return $arreglo;
}
 ?>
