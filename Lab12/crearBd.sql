IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Materiales')

DROP TABLE Materiales 

CREATE TABLE Materiales 
( 
  Clave numeric(5) not null, 
  Descripcion varchar(50), 
  Costo numeric (8,2) 
) 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proveedores')

DROP TABLE Proveedores 

CREATE TABLE Proveedores 
( 
  RFC char(13) not null, 
  RazonSocial varchar(50) 
) 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Proyectos')

DROP TABLE Proyectos 

CREATE TABLE Proyectos 
( 
  Numero numeric(5) not null, 
  Denominacion varchar(50) 
) 

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Entregan')

DROP TABLE Entregan

CREATE TABLE Entregan 
( 
  Clave numeric(5) not null, 
  RFC char(13) not null, 
  Numero numeric(5) not null, 
  Fecha DateTime not null, 
  Cantidad numeric (8,2) 
) 

BULK INSERT a1700854.a1700854.[Materiales] 
  FROM 'e:\wwwroot\a1700854\materiales.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

BULK INSERT a1700854.a1700854.[Proyectos] 
  FROM 'e:\wwwroot\a1700854\proyectos.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

BULK INSERT a1700854.a1700854.[Proveedores] 
  FROM 'e:\wwwroot\a1700854\proveedores.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

SET DATEFORMAT dmy 

BULK INSERT a1700854.a1700854.[Entregan] 
  FROM 'e:\wwwroot\a1700854\Entregan.csv' 
  WITH 
  ( 
    CODEPAGE = 'ACP', 
    FIELDTERMINATOR = ',', 
    ROWTERMINATOR = '\n' 
  ) 

  INSERT INTO Materiales values(1000,'xxx',1000)
  Delete From Materiales where Clave = 1000 and costo = 1000
  ALTER TABLE Materiales add constraint llaveMateriales PRIMARY KEY(Clave)
  INSERT INTO Materiales values(1000,'xxx',1000)
  sp_help Materiales
  sp_helpconstraint materiales

  sp_help Proveedores;
  ALTER TABLE Proveedores add constraint llaveProveedores PRIMARY KEY(RFC);
  sp_helpconstraint proveedores;

  sp_help Proyectos;
  ALTER TABLE Proyectos add constraint llaveProyectos PRIMARY KEY(Numero);
  sp_helpconstraint proyectos;

  sp_help Entregan;
  ALTER TABLE Entregan add constraint llaveEntregan PRIMARY KEY(Clave,RFC,Numero,Fecha);
  sp_helpconstraint Entregan;


  SELECT * FROM Entregan
  SELECT * FROM Materiales
  SELECT * FROM Proveedores
  SELECT * FROM Proyectos

  INSERT INTO entregan values (0,'xxx',0,'1-jan-02',0);
  Delete From Entregan where Clave = 0
  ALTER TABLE Entregan add constraint cfentreganclave foreign key (clave) references materiales(clave);
  INSERT INTO entregan values (0,'xxx',0,'1-jan-02',0);
  ALTER TABLE Entregan add constraint cfentreganrfc foreign key (rfc) references proveedores(rfc);
  ALTER TABLE Entregan add constraint cfentregannumero foreign key (numero) references proyectos(numero);
  sp_helpconstraint proyectos;
  sp_helpconstraint materiales;
  sp_helpconstraint proveedores;
  sp_helpconstraint entregan;

  INSERT INTO entregan values (1000,'AAAA800101',5000,GETDATE(),0);
  SELECT * FROM Entregan;
  Delete from Entregan where Cantidad = 0;
  ALTER TABLE entregan add constraint cantidad check (cantidad > 0);

   sp_help Proveedores
   sp_help Materiales
   sp_help Proyectos
   sp_help Entregan




