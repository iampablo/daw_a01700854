<?php
    function connectDb(){
        $servername = "localhost";
        $username = "root";
        $password = "root";
        $dbname = "asociarse1";
    
        $mysql = mysqli_connect($servername, $username, $password, $dbname);
        $mysql->set_charset("utf8");
    
        //Check connection
        if (!$mysql){
            die("Connection failed: ".mysqli_connect_error());
        }
    
        return $mysql;
    }
    
    function closeDb($mysql){
        mysqli_close($mysql);
    }
    
    function getCursos(){
        
        $db = ConnectDb() ;   
        $query  = 'SELECT * FROM Cursos' ;
        $result = $db->query($query) ;

        $numeroCursos = 0 ;
        while($row = mysqli_fetch_array($result, MYSQLI_BOTH)){
            $arreglo[$numeroCursos] = [
                //Index to access properties of the course. 
                "idCurso" => $row[0],
                "nombreCurso" => $row[1] ,
                "descripcion" => $row[2],
                "objetivos" => $row[3], 
                "ubicacion" => $row[4], 
                "dirigido" => $row[5],
                "precio" => $row[6],
                "Img" => $row[7]
            ] ;
            $numeroCursos++ ;
        }
        CloseDb($db) ;
        return $arreglo ;
    }
    
    function printCursos($courses) {
        $output = "";
        foreach($courses as $course) {
            $output .= 
            '
            <div class="col-md-3">
            <!--Modificar item-1-->
            <div class="item item-1">
                <div class="item-overlay">
                </div>
                <div class="item-content">
                    <div class="item-top-content">
                        <div class="item-top-content-inner">
                            <div class="item-product">
                                <div class="item-top-title">
                                    <h5>'.$course["nombreCurso"].'</h5>  <!-- PRODUCT TITLE-->
                                    <p class="subdescription"></p> <!-- PRODUCT DESCRIPTION-->
                                </div>
                            </div>
                            <div class="item-product-price">
                                <!-- Editar -->
                                <a class="btn" href = "editar.php?id='.$course["idCurso"].'">'. "Editar" .'</a>
                                <!-- Eliminar -->
                                <!--Para llamar a distintos modales tienes que ponerle el id de tu tabla-->
                                <span class="green-Text"><a href="#" data-toggle="modal" data-target="#myModal'.$course["idCurso"].'">Eliminar</a></span>
                                <br>
                                <!-- TERMINAR EDITAR -->
                            </div>
                        </div>
                    </div>

                    <!-- ITEM HOVER CONTENT-->
                    <div class="item-add-content">
                        <div class="item-add-content-inner">
                            <div class="section">
                                <p>
                                    
                                </p>
                            </div>
                            <div class="section">

                                <!--Aquí debe de hacer algo.-->
                                <a href="#" class="btn btn-primary custom-button red-btn">Inscribir</a><br/>

                                <!--Aquí-->
                                <a class="btn btn-primary custom-button blue-btn" href="#" >Ver Detalles</a>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- / END FIRST ITEM -->


            ';
        }
        return $output ;
    }
?>