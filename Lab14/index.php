<?php
    include_once("util.php");
    echo getPrecio();
    echo getFecha();
    echo "<br>";
    echo "¿Qué es ODBC y para qué es útil?";
    echo "<br>";
    echo "Conectividad abierta de bases de datos: El objetivo de ODBC es hacer posible el acceder a cualquier dato desde cualquier aplicación, sin importar qué sistema de gestión de bases de datos (DBMS) almacene los datos.";
    echo "<br>";
    echo "<br>";
    echo "¿Qué es SQL Injection?";
    echo "<br>";
    echo "El sql injection es un tipo de ataque a una base de datos en el cual, por la mala filtración de las variables se puede inyectar un código creado por el atacante al propio código fuente de la base de datos.";
    echo "<br>";
    echo "<br>";
    echo "¿Qué técnicas puedes utilizar para evitar ataques de SQL Injection?";
    echo "<br>";
    echo "Usa sentencias preparadas y consultas parametrizadas,Usar MySQLi,NO USES SENTENCIAS DINÁMICAS NI FUNCIONES mysql_*";
    echo "<br>";
    echo "<br>";
    echo "https://www.redeszone.net/seguridad-informatica/inyeccion-sql-manual-basico/";
?>
