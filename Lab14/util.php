<?php

function connectDB(){
  $servername= "localhost";
  $username = "root";
  $password = "root";
  $dbname = "laboratorio";

  $mysql = mysqli_connect($servername, $username, $password, $dbname);

  // Check connection
  if (!$mysql) {
      die("Connection failed: " . mysqli_connect_error());
  }
  return $mysql;
}

function closeDB($mysql){
  mysqli_close($mysql);

}

function getCursos($Cursos){
    $db = connectDB();
    $sql = 'SELECT * FROM '.$Cursos;
    $result = $db->query($sql);
    while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)){
        for($i=0; $i<count($row); $i++){
            echo $fila[$i].' ';
        }
        echo '<br>';
    }
    echo '<br>';
    mysqli_free_result($result);
    closeDB($db);
}

function getPrecio(){
    $db = connectDb();
    $sql = "SELECT Nombre_Curso, Precio, Fecha, Objetivo FROM Cursos WHERE Precio > 600";
    $result = $db->query($sql);
    $table = "
    <table border = '1px'>
        <thread>
            <tr>
                <th>Curso</th>
                <th>Precio</th>
                <th>Fecha</th>
                <th>Objetivo</th>

            </tr>
        </thread>
        <tbody>";

    while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)){

        $table .= '
          <tr>
              <td>'.$row["Nombre_Curso"].'</td>
              <td>'.$row["Precio"].'</td>
              <td>'.$row["Fecha"].'</td>
              <td>'.$row["Objetivo"].'</td>
          </tr>';
    }

    mysqli_free_result($result);
    closeDb($db);
    $table .= "</tbody></table>";
    echo "<br>";
    return $table;
}

function getFecha(){
    $db = connectDb();
    $sql = "SELECT Nombre_Curso, Precio, Fecha, Objetivo FROM Cursos WHERE Fecha between '2017/10/01' AND '2017/10/31'";
    $result = $db->query($sql);
    echo "<br>";
    $table = "
    <table border = '1px'>
        <thread>
            <tr>
                <th>Curso</th>
                <th>Precio</th>
                <th>Fecha</th>
                <th>Objetivo</th>

            </tr>
        </thread>
        <tbody>";

    while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)){

        $table .= '
          <tr>
              <td>'.$row["Nombre_Curso"].'</td>
              <td>'.$row["Precio"].'</td>
              <td>'.$row["Fecha"].'</td>
              <td>'.$row["Objetivo"].'</td>
          </tr>';
    }

    mysqli_free_result($result);
    closeDb($db);
    $table .= "</tbody></table>";
    return $table;
}

 ?>
