-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 24-10-2017 a las 03:47:13
-- Versión del servidor: 5.6.35
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `Laboratorio`
--
-- Procedure
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `crearCurso` (IN `Nombre_Curso` VARCHAR(50), IN `Objetivo` VARCHAR(200))  begin
INSERT INTO Cursos (`Nombre_Curso`,`Objetivo`) VALUES (Nombre_Curso,Objetivo);
end$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Cursos`
--

CREATE TABLE `Cursos` (
  `Id_Curso` int(10) NOT NULL,
  `Nombre_Curso` varchar(50) NOT NULL,
  `Precio` int(10) NOT NULL,
  `Fecha` date NOT NULL,
  `Objetivo` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Cursos`
--

INSERT INTO `Cursos` (`Id_Curso`, `Nombre_Curso`, `Precio`, `Fecha`, `Objetivo`) VALUES
(1, 'Liderazgo', 500, '2017-10-14', 'Aprende técnicas...'),
(2, 'bjhjhjjhhksd', 0, '0000-00-00', 'hbkjhbjkehj w'),
(3, 'Liderazgo 5', 0, '0000-00-00', 'Hola...'),
(4, 'Liderazgo 3', 0, '0000-00-00', 'APrende nuevas técnicas ');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Cursos`
--
ALTER TABLE `Cursos`
  ADD PRIMARY KEY (`Id_Curso`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Cursos`
--
ALTER TABLE `Cursos`
  MODIFY `Id_Curso` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;