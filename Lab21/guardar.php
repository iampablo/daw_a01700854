<?php
    session_start();
    require_once("modelo.php");
    if(isset($_POST["Id_Curso"])) {
        editarCurso($_POST["Nombre_Curso"], $_POST["Objetivo"], $_POST["Id_Curso"]);
        $_SESSION["mensaje"] = $_POST["nombre"].' se actualizó correctamente';
    } else {
        guardarCurso($_POST["Nombre_Curso"], $_POST["Objetivo"]);
        $_SESSION["mensaje"] = $_POST["nombre"].' se registró correctamente';
    }
    header("location:cursos.php");
    include("footer.html");
?>