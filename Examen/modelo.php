<?php
 function connectDB(){
    $servername= "localhost";
    $username = "root";
    $password = "root";
    $dbname = "Zombis";
  
    $mysql = mysqli_connect($servername, $username, $password, $dbname);
    // Check connection
    if (!$mysql) {
        die("Connection failed: " . mysqli_connect_error());
    }
    return $mysql;
  }
  
  function closeDB($mysql){
    mysqli_close($mysql);
  }
   

function AgregarZombi($Nombre,$Estado,$Registro,$Transicion){
    $db = ConnectDB();
    // insert command specification 
    $query='INSERT INTO Zombis (`Nombre`,`Estado`) VALUES (?,?)';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("ss", $Nombre,$Estado)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // Executing the statement
    if (!$statement->execute()) {
        die("Execution failed: (" . $statement->errno . ") " . $statement->error);
     } 
    closeDB($db);

}

function editarZombie($Nombre,$Estado,$Registro,$Transicion,$idZombie)
{
    $db = ConnectDB();
    // insert command specification 
    $query='UPDATE Zombies SET Nombre = ?, Estado = ? WHERE Zombis.idZombie = ?';
    // Preparing the statement 
    if (!($statement = $db->prepare($query))) {
        die("Preparation failed: (" . $db->errno . ") " . $db->error);
    }
    // Binding statement params 
    if (!$statement->bind_param("ssi", $Nombre,$Estado,$idZombie)) {
        die("Parameter vinculation failed: (" . $statement->errno . ") " . $statement->error); 
    }
    // update execution
    if ($statement->execute()) {
        echo 'There were ' . mysqli_affected_rows($mysql) . ' affected rows';
    } else {
        die("Update failed: (" . $statement->errno . ") " . $statement->error);
    }
         
    closeDB($db);
}

function getRegistroZombi($idZombie)
{
    $db = ConnectDb() ;
    //Specification of the SQL query
    $query='SELECT * FROM Zombis WHERE `idZombie` ="'.$idZombie.'"';
    // Query execution; returns identifier of the result group
    $result = $db->query($query);   
    $fila = mysqli_fetch_array($result, MYSQLI_BOTH);
    CloseDb($db) ;
    return $fila; 
}

function getFecha(){
    $db = connectDb();
    $sql = "SELECT Nombre,Estado,Registro,Transicion FROM Zombis WHERE Registro '";
    $result = $db->query($sql);
    echo "<br>";
    $table = "
    <table border = '1px'>
        <thread>
            <tr>
                <th>Cantidad de Zombis</th>
                <th>Zombies con Infeccion</th>
                <th>Zombies en Coma</th>
                <th>Zombies en transformacion</th>
                <th>Zombies no muertos</th>
            </tr>
        </thread>
        <tbody>";

    while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)){

        $table .= '
          <tr>
              <td>'.$row["idZombie"].'</td>
              <td>'.$row["idZombie"].'</td>
              <td>'.$row["idZombie"].'</td>
              <td>'.$row["idZombie"].'</td>
              <td>'.$row["idZombie"].'</td>
          </tr>';
    }

    mysqli_free_result($result);
    closeDb($db);
    $table .= "</tbody></table>";
    return $table;
}


function getZombies() {
    $db = connectDB();
    //Specification of the SQL query
    $query='SELECT * FROM Zombis ORDER BY Registro asc';
     // Query execution; returns identifier of the result group
    $result = $db->query($query);
    $cuadros = "";
     // cycle to explode every line of the results
    while ($fila = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
    $cuadros .= '
    <div class="row wow fadeInLeft animated" data-wow-offset="30" data-wow-duration="1.5s" data-wow-delay="0.15s">
    <div class="col-md-3">
    <div class="item item-1" style="background-image: url(images/products/'.$fila["idZombie"].'.jpg);">
        <div class="item-overlay">
        </div>
        <div class="item-content">
            <div class="item-top-content">
                <div class="item-top-content-inner">
                    <div class="item-product">
                      <div class="item-top-title">
                        <span class="item-top-title">'.$fila["Nombre"].'</span>
                                 <p>'.$fila["Estado"].'</p>
                                 <p>'.$fila["Registro"].'</p>
                                 <p class="subdescription">
                            </p> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-add-content">
                <div class="item-add-content-inner">
                    <div class="section">
                        <p>
                        <p>'.$fila["Transicion"].'</p>
                        </p>
                    </div>
                    <div class="section">
                        <a class=class="btn btn-primary custom-button red-btn" href="editar.php?id='.$fila["idZombie"].'">'.Editar.'</a>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
</div>';
}
    // it releases the associated results
    mysqli_free_result($result);
    closeDB($db);
    return $cuadros;
}

function getDropdownEstado($table, $order_by_field, $selected=-1) {
    $db = connectDb();
    //Specification of the SQL query
    $query='SELECT * FROM '.$table.' ORDER BY '.$order_by_field;
     // Query execution; returns identifier of the result group
    $result = $db->query($query);
    $select = '<select name="Nombre_Curso" id="Nombre_Curso">';
     // cycle to explode every line of the results
    while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
                                                // Options: MYSQLI_NUM to use only numeric indexes
                                                // MYSQLI_ASSOC to use only name (string) indexes
                                                // MYSQLI_BOTH, to use both
        if ($selected == $row["idZombie"]) {
            $select .= '<option value="'.$row["Muerto"].'" selected>'.$row["$order_by_field"].'</option>';
        } else {
            $select .= '<option value="'.$row["Coma"].'">'.$row["$order_by_field"].'</option>';
        }
    }
    $select .= '</select><label for="'.$table.'">Nombre_Curso</label>';
    // it releases the associated results
    mysqli_free_result($result);
    closeDb($db);
    return $select;
}
?>