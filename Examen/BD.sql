-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 27-10-2017 a las 22:54:18
-- Versión del servidor: 5.6.35
-- Versión de PHP: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `Zombis`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Zombis`
--

CREATE TABLE `Zombis` (
  `idZombie` int(3) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `Estado` varchar(50) NOT NULL,
  `Registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Transicion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Muerto` varchar(11) NOT NULL,
  `Infectado` varchar(11) NOT NULL,
  `Coma` varchar(11) NOT NULL,
  `Transformandose` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Zombis`
--

INSERT INTO `Zombis` (`idZombie`, `Nombre`, `Estado`, `Registro`, `Transicion`, `Muerto`, `Infectado`, `Coma`, `Transformandose`) VALUES
(7, 'Pablo Pozos', 'Muerto', '2017-10-27 20:35:32', '2017-10-27 20:35:32', '0', '0', '0', '0'),
(12, 'Juan Perez', 'Vivo', '2017-10-27 20:50:26', '2017-10-27 20:50:26', '0', '0', '0', '0'),
(13, 'Karina Reyes', 'Muerto', '2017-10-27 20:50:40', '2017-10-27 20:50:40', '0', '0', '0', '0'),
(14, 'Rafael Ortiz', 'Coma', '2017-10-27 20:50:48', '2017-10-27 20:50:48', '0', '0', '0', '0');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Zombis`
--
ALTER TABLE `Zombis`
  ADD PRIMARY KEY (`idZombie`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Zombis`
--
ALTER TABLE `Zombis`
  MODIFY `idZombie` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;