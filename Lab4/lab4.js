function cuadrado(numero) {
  var table = document.createElement("table");
  var tbody = document.createElement("tbody");
  table.appendChild(tbody);
  var n = prompt ("Ingrese numero");
  for (var i=1;i <= n;i++){
    let row = document.createElement("tr");
    let td = document.createElement("td");
    td.appendChild(document.createTextNode("Numero "+i));
    row.appendChild(td);
    td = document.createElement("td");
    td.appendChild(document.createTextNode("Cuadrado "+i*i));
    row.appendChild(td);
    td = document.createElement("td");
    td.appendChild(document.createTextNode("Cubo "+i*i*i));
    row.appendChild(td);
    tbody.appendChild(row);
  }
  document.body.appendChild(table);
}

function aleatorio(){
      var n1 = Math.floor(Math.random() * 100 +1);
      var n2 = Math.floor(Math.random() * 100 +1);
      var tiempoa = new Date();
      var segundos = tiempoa.getSeconds();

      var r = prompt("Inserta suma " + n1 + " y " + n2);
      var tiempob = new Date();
      var rs = tiempob.getSeconds();
      var tiempo = rs - segundos;

      if (r == n1+n2) {
        alert("Correcto y tu tiempo es: " + tiempo + " segundos");
      } else {
        alert('Incorrecto y tu tiempo es: ' + tiempo + " segundos");
      }
}

function contador(arreglo){
  var neg = 0;
	var pos = 0;
	var cer = 0;
		for (var i = arreglo.length -1; i>= 0; i--){
			if(arreglo[i] > 0){
				pos++;
			}
			else if (arreglo[i] === 0){
				cer++;
			}
			else{
				neg++;
			}
		}
	alert(+neg+ " negativos, " +pos+ " positivos, y "
			+cer+ " ceros en: " +arreglo+ ".");
}
function funcon(){
  var arr = [1, 0, 0, -3, -10, -1, 4, 5];
  contador(arr);
}

function promedios(arreglo) {
	let promedios = new Array() ;
  let promedio = 0 ;
  let ultimo = 0 ;
  for(let i = 0;  i < arreglo.length ; i++){
    for(let j = 0 ; j < arreglo[i].length ; j++){
      promedio += arreglo[i][j] ;
      ultimo++ ;
      if(ultimo == arreglo[i].length){
        promedios.push(promedio / arreglo[i].length) ;
        ultimo = 0 ;
        promedio = 0 ;
      }
    }
    alert("Promedio: " +promedios+ " de la matriz: " +arreglo[i]);
  }
}
function funpro(){
  let arre = [[20, 1, 4],[5, 7, 9],[9, 8, 2]];
  promedios(arre);
}

function inverso(x){
  let numeros = x.toString();
	numeros = numeros.split("").reverse().join("");
  alert("El numero es: " +x);
  alert("Los numero inversos son: " +numeros);
}
function funinverso(){
	var y = Math.floor(Math.random()*999 + 1);
	console.log(y);
	inverso(y);
}
function palindromo(cadena) {
  var resultado = "La frase \""+cadena+"\" \n";
  var cadenaOriginal = cadena.toLowerCase();
  var letrasEspacios = cadenaOriginal.split("");
  var cadenaSinEspacios = "";
  for(i in letrasEspacios) {
    if(letrasEspacios[i] != " ") {
      cadenaSinEspacios += letrasEspacios[i];
    }
  }
  var letras = cadenaSinEspacios.split("");
  var letrasReves = cadenaSinEspacios.split("").reverse();
  var iguales = true;
  for(i in letras) {
    if(letras[i] == letrasReves[i]) {
    }
    else {
      iguales = false;
    }
  }
  if(iguales) {
    resultado += " es un palíndromo";
  }
  else {
    resultado += " no es un palíndromo";
  }
  return resultado;
}

function pali(){
  alert(palindromo("Anita lava la tina"));
  alert(palindromo("Bob"));
  alert(palindromo("Hola Amigos"));
  alert(palindromo("Isaac no ronca asi"));
}
