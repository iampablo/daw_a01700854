BULK INSERT a1700854.a1700854.[Materiales]
   FROM 'e:\wwwroot\a1700854\materiales.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )
SELECT  * FROM Materiales