SET DATEFORMAT dmy
BULK INSERT a1700854.a1700854.[Entregan]
   FROM 'e:\wwwroot\a1700854\entregan.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )
SELECT  * FROM Entregan