BULK INSERT a1700854.a1700854.[Proveedores]
   FROM 'e:\wwwroot\a1700854\proveedores.csv'
   WITH 
      (
         CODEPAGE = 'ACP',
         FIELDTERMINATOR = ',',
         ROWTERMINATOR = '\n'
      )
SELECT  * FROM Proveedores