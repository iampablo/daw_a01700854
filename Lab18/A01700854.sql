--La suma de las cantidades e importe total de todas las entregas realizadas durante el 97.
SET DATEFORMAT DMY
SELECT SUM(E.Cantidad) AS TotalCantidad, SUM(1 + M.Costo*E.Cantidad) AS TotalCosto
FROM Materiales M, Entregan E
WHERE M.Clave = E.Clave
AND Fecha BETWEEN '1/01/1997' AND '31/12/1997'

-- Para cada proveedor, obtener la razón social del proveedor, número de entregas e importe total de las entregas realizadas.
SELECT P.RazonSocial,COUNT(E.RFC) AS Entregados,SUM(M.Costo * E.Cantidad) as 'Total'
FROM Materiales M, Entregan E,Proveedores P
WHERE M.Clave = E.Clave
AND P.RFC = E.RFC
GROUP BY E.RFC, RazonSocial

-- Por cada material obtener la clave y descripción del material, la cantidad total entregada
-- la mínima cantidad entregada, la máxima cantidad entregada, el importe total de las entregas de aquellos materiales
-- en los que la cantidad promedio entregada sea mayor a 400.
SELECT M. Clave,M.Descripcion,SUM(E.Cantidad)as Total ,Min(E.Cantidad) as Min ,Max(E.Cantidad) as Max, SUM(E.Cantidad*(M.Costo + M.PorcentajeImpuesto)) as Importe
FROM  Entregan E, Materiales M
Where E.Clave=M.Clave
GROUP BY m.Clave,m.Descripcion
HAVING AVG(Cantidad) > 400

-- Para cada proveedor, indicar su razón social y mostrar la cantidad promedio de cada material entregado, detallando la clave y descripción del material,
-- excluyendo aquellos proveedores para los que la cantidad promedio sea menor a 500.
CREATE view Razon_clave as (
SELECT RazonSocial, AVG(e.Cantidad)AS PromedioFinal, E.Clave
FROM Proveedores P,  Entregan E WHERE E.RFC = P.RFC  GROUP BY E.RFC,RazonSocial,E.Clave)
SELECT R.RazonSocial,r.PromedioFinal,M.Clave,M.Descripcion
FROM Razon_clave R, Materiales M
WHERE M.Clave = r.Clave
AND r.PromedioFinal <= 500

-- Mostrar en una solo consulta los mismos datos que en la
--  consulta anterior pero para dos grupos de proveedores: aquellos para los que la cantidad promedio entregada es
--  menor a 370 y aquellos para los que la cantidad promedio entregada sea mayor a 450.

CREATE VIEW Razon_clave AS (
SELECT RazonSocial, AVG(e.Cantidad)AS PromedioFinal, E.Clave
FROM Proveedores AS P,  Entregan AS E WHERE E.RFC = P.RFC  GROUP BY E.RFC,RazonSocial,E.Clave)
SELECT R.RazonSocial,r.PromedioFinal,M.Clave,M.Descripcion
FROM Razon_clave R, Materiales M
WHERE M.Clave = r.Clave
AND r.PromedioFinal >= 450
ORDER BY  PromedioFinal ASC

INSERT INTO Materiales VALUES (9876,'Arena',560,2.88)
INSERT INTO Materiales VALUES (5555,'Chapopote',20,2.7)
INSERT INTO Materiales VALUES (2309,'Martillo',340,2.92)
INSERT INTO Materiales VALUES (6789,'Madera',999,2.94)
INSERT INTO Materiales VALUES (1457,'Clavo',1000,2.96)

--Clave y descripción de los materiales que nunca han sido entregados.
SELECT Clave As Clave, Descripcion As Descripción
FROM Materiales
WHERE Clave NOT IN (SELECT Clave FROM Entregan)

--Razón social de los proveedores que han realizado entregas tanto al proyecto
-- 'Vamos México' como al proyecto 'Querétaro Limpio'.
SELECT RazonSocial AS "Razón Social"
FROM Proveedores
WHERE RFC IN (SELECT E.RFC FROM Entregan AS E, Proyectos  AS P
			        WHERE E.Numero = P.Numero
              AND (P.Denominacion LIKE 'Vamos Mexico'
							OR p.denominacion LIKE 'Queretaro Limpio'))

--Descripción de los materiales que nunca han sido entregados al proyecto 'CIT Yucatán'.
SELECT Descripcion AS Descripción
FROM Materiales
WHERE Clave NOT IN (SELECT E.Clave
                    FROM Entregan AS E, Proyectos AS P
				            WHERE E.Numero = P.Numero
                    AND P.Denominacion LIKE 'CIT Yucatan')

--Razón social y promedio de cantidad entregada de los proveedores cuyo promedio de cantidad entregada
--  es mayor al promedio de la cantidad entregada por el proveedor con el RFC 'VAGO780901'.
SELECT P.Razonsocial AS "Razón Social", AVG(E.Cantidad) AS "Promedio"
FROM Proveedores AS P, Entregan AS E
WHERE P.RFC = E.RFC
GROUP BY P.Razonsocial
HAVING AVG(E.Cantidad) > (SELECT AVG(Cantidad)
                          FROM Entregan
                          WHERE RFC LIKE 'VAGO780901'
                          GROUP BY RFC)

--RFC, razón social de los proveedores que participaron en el proyecto
-- 'Infonavit Durango' y cuyas cantidades totales entregadas en el 2000 fueron mayores a las
-- cantidades totales entregadas en el 2001.
CREATE VIEW C2000 (RFC, Cantidad) AS (
	SELECT RFC, SUM(Cantidad)
  FROM Entregan AS E
  WHERE Fecha BETWEEN '01/01/2000' AND '31/12/2000'
  GROUP BY RFC
)
CREATE VIEW C2001(RFC, Cantidad) AS (
	SELECT RFC, SUM(Cantidad)
  FROM Entregan AS E
  WHERE Fecha BETWEEN '01/01/2001' AND '31/12/2001'
  GROUP BY RFC
)

SET DATEFORMAT DMY
SELECT E.RFC As RFC, Pr.RazonSocial As "Razón Social"
FROM Entregan AS E, Proveedores AS Pr, Proyectos AS P, C2000 C2000, C2001 C2001
WHERE Pr.RFC = E.RFC
AND P.Numero = E.Numero AND P.Denominacion LIKE 'Infonavit Durango'
AND E.Fecha BETWEEN '01/01/2000' AND '31/12/2000' AND C2000.RFC = C2001.RFC
AND C2000.Cantidad > C2001.Cantidad
GROUP BY E.RFC, Pr.Razonsocial