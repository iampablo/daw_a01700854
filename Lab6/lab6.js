function mouseEnter() {
    document.getElementById("cambio").style.color = "red";
}
function mouseLeave() {
    document.getElementById("cambio").style.color = "white";
}
function mouseDown() {
    document.getElementById("tex").innerHTML = "Static (comportamiento normal o por defecto), Relative (permite que un elemento se desplace respecto a lo que hubiera sido su posición normal)";
}
function mouseUp() {
    document.getElementById("tex").innerHTML = "Absolute (permite que un elemento se desplace respecto al origen de coordenadas del primer elemento contenedor posicionado ó respecto a la esquina superior izquierda de la ventana de visualización), Fixed (permite fijar un elemento en una posición respecto al origen de coordenadas del primer elemento contenedor posicionado ó respecto a la esquina superior de la ventana de visualización)";
}
function mouseDown1() {
    document.getElementById("tex1").innerHTML = "Visible y Hidden";
}
function mouseUp1() {
    document.getElementById("tex1").innerHTML = "Collapse y Inherit";
}
function mouseDown2() {
    document.getElementById("tex2").innerHTML = "El nivel de apilamiento en el actual contexto de apilado.";
}
function mouseUp2() {
    document.getElementById("tex2").innerHTML = "Si la caja establece un contexto de apilamiento.";
}


function ventana() {
    var txt;
    var frase = prompt("Ingresa texto o Nombre:", "Pablo es genial");
    if (frase == null || frase == "") {
        txt = "Cancelado";
    } else {
        txt = "Hola " + frase;
    }
    document.getElementById("text3").innerHTML = txt;
}

function TimeOut() {
    var myWindow = window.open("", "myWindow", "width=300, height=200");
    myWindow.document.write("<p> Este es mi laboratorio, Disfrutalo</p>");
    setTimeout(function(){ myWindow.close() }, 5000);
}

function allowDrop(ima) {
    ima.preventDefault();
}

function drag(ima) {
    ima.dataTransfer.setData("Imagen", ima.target.id);
}

function drop(ima) {
    ima.preventDefault();
    var imagen = ima.dataTransfer.getData("Imagen");
    ima.target.appendChild(document.getElementById(imagen));
}
