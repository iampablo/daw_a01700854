SELECT * FROM Clientes_Banca
-- ¿Que pasa cuando deseas realizar esta consulta?
-- Muestra los datos que estan en la tabla.

BEGIN TRANSACTION PRUEBA2
INSERT INTO Clientes_Banca VALUES('004','Ricardo Rios Maldonado',19000);
INSERT INTO Clientes_Banca VALUES('005','Pablo Ortiz Arana',15000);
INSERT INTO Clientes_Banca VALUES('006','Luis Manuel Alvarado',18000);

SELECT * FROM Clientes_Banca

--¿Qué pasa cuando deseas realizar esta consulta?
-- Hay un error y solo se muestran en el usuario 2

SELECT * FROM CLIENTES_BANCA where NoCuenta='001'

--Explica por qué ocurre dicho evento.
-- Solo muestra los datos con ese numero de cuenta

ROLLBACK TRANSACTION PRUEBA2

SELECT * FROM CLIENTES_BANCA

--¿Qué ocurrió y por qué?
--Elimina los datos que hacia la transaccion 2 ya que el rollback descarta todos los cambios desde el begin



