--Pablo Pozos Aguilar
--A01700854

CREATE TABLE Clientes_Banca(
  NoCuenta VARCHAR(5) PRIMARY KEY ,
  Nombre VARCHAR(30),
  Saldo NUMERIC(10,2)
);

CREATE TABLE Tipos_Movimiento(
  ClaveM VARCHAR(2) PRIMARY KEY ,
  Descripcion VARCHAR(30)
);

CREATE TABLE Movimientos(
  NoCuenta VARCHAR(5),
  ClaveM VARCHAR(2),
  Fecha DATETIME,
  Monto NUMERIC(10,2)
);

BEGIN TRANSACTION PRUEBA1
INSERT INTO Clientes_Banca VALUES('001', 'Manuel Rios Maldonado', 9000);
INSERT INTO Clientes_Banca VALUES('002', 'Pablo Perez Ortiz', 5000);
INSERT INTO Clientes_Banca VALUES('003', 'Luis Flores Alvarado', 8000);
COMMIT TRANSACTION PRUEBA1

SELECT * FROM Clientes_Banca

ROLLBACK TRANSACTION PRUEBA2
--------------------------------------------------------

BEGIN TRANSACTION PRUEBA3
INSERT INTO TIPOS_MOVIMIENTO VALUES('A','Retiro Cajero Automatico');
INSERT INTO TIPOS_MOVIMIENTO VALUES('B','Deposito Ventanilla');
COMMIT TRANSACTION PRUEBA3

------------------------------------------------------------

BEGIN TRANSACTION PRUEBA4
INSERT INTO Movimientos VALUES('001','A',GETDATE(),500);
UPDATE Clientes_Banca SET SALDO = SALDO -500
WHERE NoCuenta='001'
COMMIT TRANSACTION PRUEBA4

--------------------------------------------------------------

BEGIN TRANSACTION PRUEBA5
INSERT INTO CLIENTES_BANCA VALUES('005','Rosa Ruiz Maldonado',9000);
INSERT INTO CLIENTES_BANCA VALUES('006','Luis Camino Ortiz',5000);
INSERT INTO CLIENTES_BANCA VALUES('001','Oscar Perez Alvarado',8000);

IF @@ERROR = 0
COMMIT TRANSACTION PRUEBA5
ELSE
BEGIN
PRINT 'A transaction needs to be rolled back'
ROLLBACK TRANSACTION PRUEBA5
END

--¿Para qué sirve el comando @@ERROR revisa la ayuda en línea?
-- Si es igual a cero significa que la transaccion no tuvo errores.

--¿Explica qué hace la transacción?
--Agrega 3 datos nuevos y verifica si hay algun error en la transaccion y si lo hay no modifica nada.

--¿Hubo alguna modificación en la tabla? Explica qué pasó y por qué.
--No hubo ninguna, se presento un error y no modifico nada ya que una clave ya existia.

----------------------------------------------------------------------------------------

---Retiro de cajero
IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'Retiro_Cajero' AND type = 'P')
    DROP PROCEDURE Retiro_Cajero
GO

CREATE PROCEDURE Retiro_Cajero
    @unocuenta VARCHAR(5),
    @umonto NUMERIC(10,2)
AS
    BEGIN TRANSACTION RETIRO
	INSERT INTO Movimientos VALUES(@unocuenta,'A',GETDATE(),@umonto);
	UPDATE Clientes_Banca SET SALDO = SALDO - @umonto
	WHERE NoCuenta = @unocuenta

	IF @@ERROR = 0
	  COMMIT TRANSACTION RETIRO
	ELSE
	BEGIN
	  PRINT 'A transaction needs to be rolled back'
	  ROLLBACK TRANSACTION RETIRO
	END
GO

EXECUTE Retiro_Cajero '003',800

SELECT * FROM Movimientos
SELECT * FROM Clientes_Banca

----------------------------------------------------------------------------------------
--Deposito en ventanilla

IF EXISTS (SELECT name FROM sysobjects
    WHERE name = 'Deposito_Ventanilla' AND type = 'P')
    DROP PROCEDURE Deposito_Ventanilla
GO

CREATE PROCEDURE Deposito_Ventanilla
    @unocuenta VARCHAR(5),
    @umonto NUMERIC(10,2)
AS
    BEGIN TRANSACTION DEPOSITO
	INSERT INTO Movimientos VALUES(@unocuenta,'B',GETDATE(),@umonto);
	UPDATE Clientes_Banca SET SALDO = SALDO + @umonto
	WHERE NoCuenta = @unocuenta

	IF @@ERROR = 0
	COMMIT TRANSACTION DEPOSITO
	ELSE
	BEGIN
	PRINT 'A transaction needs to be rolled back'
	ROLLBACK TRANSACTION DEPOSITO
	END
GO

EXECUTE Deposito_Ventanilla '002',1000
EXECUTE Deposito_Ventanilla '001',300

SELECT * FROM Movimientos
SELECT * FROM Clientes_Banca
